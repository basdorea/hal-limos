# HAL-LIMOS

## Outils permettant la recupération et le formatage des publis à partir de HAL

Ces scripts ont été réalisés en Juillet 2018 et Janvier 2019

Ils permettent de récupérer à partir de HAL, les publications d'une collection et de les rentrer dans une base puis de leur attribuer une note à partir d'organismes de ranking.

Les organismes de ranking que l'on utilise

SJR, généraliste, ranke les articles : https://www.scimagojr.com/

CORE, domaine informatique, ranke les conférences : http://portal.core.edu.au/conf-ranks/

Ces scripts sont lancés chaque nuit dans cet ordre :

* script_populate_publications_renew.py -> supprime toutes les publis de la base et les réimporte à partir de HAL
* script_create_csv_scimago_core.py -> récupère les bases de données de scimago et Core et crée des fichiers CSV ad hoc
* script_note_publications.py -> compare les infos entre les CSV et les journaux et conférences des publis enregistrées en base et leur attribue une note quand c'est possible


## Outils permettant les imports massifs dans HAL

Ces scripts ont été réalisés en Février 2019

Le script sword/script\_list\_to_hal.py permet de rentrer des publis d'un chercheur dans HAL à partir d'une liste formatée de type HCERES.

```RI13 - DUMONT B., HILL D., "Multi-agent simulation of group foraging in sheep: effects of spatial memory, conspecific attraction and plot size.", Ecological Modelling, Vol. 141, pp. 201-215, 2001,```

Ce script parse la liste sword/all\_publis\_user.txt , récupère les données, génère un fichier XML par ligne (soit par publi) et génère un fichier CSV

Selon les variables renseignées dans les premières lignes du script, un dépôt sera fait dans la base de préprodcution de HAL ou directement dans la base de production.

Il est bien sur nécessaier d'avoir des comptes dans les bases correspondantes.


## Deploiement pour le script d'import sword/script\_list\_to\_hal.py et sword/script\_dblp\_to_hal.py

Création d'un environnement virtuel

```bash
sudo apt-get install python-pip
pip install --user virtualenv
mkdir halenv
virtualenv /home/myself/halenv

source halenv/bin/activate
```

Clone du dépôt à partir de gitlab

```bash
cd halenv
git clone https://gitlab.limos.fr/basdorea/hal-limos.git
```

Téléchargement des bibliothèques

```bash
cd HAL-LIMOS
pip install -r requirements.txt
```

Lancement du script en python 2.7

```bash
cd sword
python script_list_to_hal.py
```

ou 

```bash
cd sword
python script_dblp_to_hal.py
```