#!/usr/bin/python
#-*- coding: utf-8 -*-

#from __future__ import unicode_literals
import requests
import csv

# lib XML
from lxml import etree

# lib bibtex
import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import homogenize_latex_encoding
from bibtexparser.customization import convert_to_unicode

#################
## VARIABLES
#################

# Auteur des publis
bib_name_user = "Pascal Lafourcade"
name_user = "Lafourcade"
firstname_user = "Pascal"
labo_auth_final = "struct-490706"
id_hal_user=""
login_user = "pascal-lafourcade"


# Domaines de l'auteur
listdomains = []
# listdomains.append("info.info-dm")
listdomains.append("info.info-se")# Genie logiciel
#listdomains.append("info.info-mo")# Modelisation et simulation
#listdomains.append("info.info-dc")# Calcul parallele, distribué et partegé
listdomains.append("info.info-cr")# Crypto 

# Compte de depot
login_depot = "bdoreau"
passwd_depot = "mbrv1232"

# booleens depots
bool_depot_preprod = False
bool_depot_prod = False

# Fichier bibtex contenant les publis
bibtex_list ="Lafourcade_Pascal.bib"
# Fichier CSV en sortie
file_publis_csv = "all_csv_lafourcade.csv"




#############################################################################################################################
###################################           BUILD XML ARTICLE CONF POSTER OUV COUV
# CREATION XML pour articles, conferences et posters, ouvrage et chapitres d'ouvrage
#############################################################################################################################
#############################################################################################################################

def createXml_sendHal(numero,listauthors, lang_title, title_publi, name_conf, nb_pages, date_pub, listdomains, type_pub, ville, pays, pays_acr, doi_value, editor_book, volume):

    ## VARIABLES CREATION XML
    global name_user
    global firstname_user
    global labo_auth_final
    global id_hal_user
    global login_user

    global login_depot
    global passwd_depot

    global bool_depot_preprod
    global bool_depot_prod

    # Recup des donnes de la liste
    namefile = str(numero)+"publi.xml"

    file = open(namefile,"w") 
    file.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")


    tei = etree.Element("TEI")
    tei.set("xmlns","http://www.tei-c.org/ns/1.0")
    tei.set("xmlns_hal","http://hal.archives-ouvertes.fr/")   ### ATTENTION   remplacement xmlns_hal par xmlns:hal

    text = etree.SubElement(tei, "text")
    body = etree.SubElement(text, "body")
    listBibl = etree.SubElement(body, "listBibl")
    biblFull = etree.SubElement(listBibl, "biblFull")

    # TitleStmt
    if (type_pub == "COMM") or (type_pub == "POSTER") or  (type_pub == "ART") or (type_pub == "OUV") or  (type_pub == "COUV")  :

        titleStmt = etree.SubElement(biblFull, "titleStmt")
        title = etree.SubElement(titleStmt, "title")
        title.set("xml_lang",lang_title)          #Build from data      ### ATTENTION  remplacement xml_lang par xml:lang
        title.text = title_publi#.decode("utf-8")                   #Build from data

        for auth in listauthors :
            author = etree.SubElement(titleStmt, "author")
            author.set("role","aut")
            persName = etree.SubElement(author, "persName")
            forename = etree.SubElement(persName, "forename")
            forename.set("type","first")
            forename.text = auth[1]                #Build from data
            surname = etree.SubElement(persName, "surname")
            surname.text = auth[0]                    #Build from data
            if auth[2] is not "" :
                affiliation = etree.SubElement(author, "affiliation")
                labo_author = "#"+auth[2]
                affiliation.set("ref",labo_author)             #Build from data
                if auth[1] == name_user and id_hal_user is not "" :
                    idno_idhal =  etree.SubElement(author, "idno")
                    idno_idhal.set("type", "idhal")
                    idno.text = id_hal_user

    # Unnecessary noteStmt
    """
    notesStmt = etree.SubElement(biblFull, "notesStmt")
    note = etree.SubElement(notesStmt, "note")
    note.set("type","commentary")
    note.text = comment_publi
    """
    # sourceDesc
    sourceDesc = etree.SubElement(biblFull, "sourceDesc")
    biblStruct = etree.SubElement(sourceDesc, "biblStruct")
    analytic = etree.SubElement(biblStruct, "analytic")
    title = etree.SubElement(analytic, "title")
    title.set("xml_lang",lang_title)          #Build from data             ### ATTENTION remplacement  xml_lang par xml:lang
    title.text = title_publi#.decode("utf-8")                   #Build from data

    for auth in listauthors :
        author = etree.SubElement(analytic, "author")
        author.set("role","aut")
        persName = etree.SubElement(author, "persName")
        forename = etree.SubElement(persName, "forename")
        forename.set("type","first")
        forename.text = auth[1]                #Build from data
        surname = etree.SubElement(persName, "surname")
        surname.text = auth[0]                    #Build from data
        if auth[2] is not "" :
            affiliation = etree.SubElement(author, "affiliation")
            labo_author = "#"+auth[2]
            affiliation.set("ref",labo_author)             #Build from data
            if auth[1] == name_user:
                idno_idhal =  etree.SubElement(author, "idno")
                idno_idhal.set("type", "idhal")
                idno.text = id_hal_user



    monogr = etree.SubElement(biblStruct, "monogr")
    title = etree.SubElement(monogr, "title")
    if type_pub == "ART":
        title.set("level","j")
        title.text = name_conf#.decode("utf-8")                       #Build from data
    if type_pub == "COMM":
        title.set("level","m")
        title.text = name_conf#.decode("utf-8")                       #Build from data
    if type_pub == "POSTER":
        title.set("level","m")
        title.text = name_conf#.decode("utf-8")    
    # Pour les COUV on donne le titre du livre (ici noté name_conf)
    if type_pub == "COUV":
        title = etree.SubElement(monogr, "title")
        title.set("level","m")
        title.text = name_conf#.decode("utf-8")   # ici name_conf = titre livre                    #Build from data
        if editor_book is not "" :
            editor = etree.SubElement(monogr, "editor")
            editor.text = editor_book#.decode("utf-8")     #Build from data

    
    if (type_pub == "COMM") or (type_pub == "POSTER") :
        if (pays.strip() is not ""):
    
            meeting = etree.SubElement(monogr, "meeting")
            title = etree.SubElement(meeting, "title")
            title.text = name_conf#.decode("utf-8")                      #Build from data

            date = etree.SubElement(meeting, "date")
            date.set("type","start")
            date.text = date_pub
            settlement = etree.SubElement(meeting, "settlement")
            settlement.text = ville#.decode("utf-8")
            country = etree.SubElement(meeting, "country")
            country.set("key",pays_acr)
            country.text = pays#.decode("utf-8")
    
    imprint = etree.SubElement(monogr, "imprint")
    
    if (volume is not "") :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","volume")
        biblScope.text = volume#.decode("utf-8")#.strip()
    if (nb_pages is not "") :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","pp")
        biblScope.text = nb_pages#.decode("utf-8")#.strip()
    
    date = etree.SubElement(imprint, "date")
    date.set("type","datePub")
    date.text = date_pub

    if doi_value is not "" :
        doi = etree.SubElement(biblStruct, "idno")
        doi.set("type","doi")
        doi.text = doi_value

    # profileDesc
    profileDesc = etree.SubElement(biblFull, "profileDesc")
    langUsage = etree.SubElement(profileDesc, "langUsage")
    language = etree.SubElement(langUsage, "language")
    language.set("ident",lang_title)                     #Build from data
    textClass = etree.SubElement(profileDesc, "textClass") 
    for domain in listdomains :
        classCode = etree.SubElement(textClass, "classCode") 
        classCode.set("scheme","halDomain")
        classCode.set("n",domain)                          #Build from data
    classCode = etree.SubElement(textClass, "classCode") 
    classCode.set("scheme","halTypology")
    classCode.set("n",type_publi)

    # back 
    back = etree.SubElement(text, "back")
    listOrg = etree.SubElement(back, "listOrg")
    listOrg.set("type","structures")
    org = etree.SubElement(listOrg, "org")
    org.set("type","laboratory")
    org.set("xml_id",labo_auth_final)                                            ### ATTENTION remplacement  xml_id par xml:id


    docxml = str(etree.tostring(tei, pretty_print=True))

    file.write(docxml) 

    file.close() 

    ## CORRECTIFS

    with open(namefile, 'r') as file :
      docxml = file.read()

    # Replace the target string
    docxml = docxml.replace("xmlns_hal=","xmlns:hal=")
    docxml = docxml.replace("xml_id=","xml:id=")
    docxml = docxml.replace("xml_lang=","xml:lang=")

    # Write the file out again
    with open(namefile, 'w') as file:
        file.write(docxml)



    headers = {
        'Packaging': 'http://purl.org/net/sword-types/AOfr',
        'Content-Type': 'text/xml',
        'On-Behalf-Of': 'login|'+login_user+';idhal|'+id_hal_user,
    }


    reponse_http = False
    response = ""

    # DEPOT PREPROD
    if bool_depot_preprod == True :
        data = open(namefile)
        response = requests.post('https://api-preprod.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot))
        print "response POST : code	",response

    # DEPOT PROD
    if bool_depot_prod == True :
        data = open(namefile)
        response = requests.post('https://api.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot))
        print "response POST : code	",response   

    print 'request POST OK'
    if "202" in str(response) :
        reponse_http = True
    
    return reponse_http


####################################################################
########## END METHOD CREATE XML
##########
####################################################################





###################################
###### COMPTEURS
###################################

cnt_article = 0
cnt_inproceeding = 0
cnt_proceeding = 0
cnt_incollection = 0
cnt_book = 0
cnt_total = 0
cnt_phdthesis = 0


# errors generic
cnt_error_auth = 0
cnt_error_title = 0

# errors articles
cnt_error_jrn = 0
cnt_error_vol = 0
cnt_error_numb = 0
cnt_error_pages = 0
cnt_error_year_art = 0
cnt_error_doi = 0

# errors inproceeding
cnt_error_booktitle = 0
cnt_error_pages = 0
cnt_error_year_inp = 0
cnt_error_crossref = 0
cnt_error_publisher = 0
cnt_error_editor = 0

# errors proceeding
cnt_error_idcrossref = 0
cnt_error_publisher_p = 0
cnt_error_editor_p = 0



###################################
###### READ OTHER FILES
###################################

# lecture du fichier dblp_acronym_csv
list_acronym_country = []
with open('dblp_acronym_country.csv', 'rb') as csvfile:
    list_acron = csv.reader(csvfile, delimiter=':', quotechar='|')
    for row in list_acron:
        #print row[0], " ",row[1]
        list_acronym_country.append((row[0], row[1]))






########################################################################################################################################
########################################################################################################################################
########## SCRIPT PRINCIPAL
########################################################################################################################################
########################################################################################################################################

resultat = ""
problemes_url = ""
problemes_doublon = ""
problemes_depot = ""
depots = ""

list_pub_csv = []

# une participation a une conf (inproceeding) peut avoir une crossref reliee a une proceeding qui est une conf, celle ci
# se trouve en fin du doc bibtex donc -> 
# on cherche crossref dans inproceeding, si on le trouve aller le chercher en bas, sinon chercher editor et publier directement dans inproceeding
# car certains chercheurs peuvent creer leur bibtex comme ceci


with open(bibtex_list) as bibtex_file:
    parser = BibTexParser()
    parser.customization = convert_to_unicode
    bib_database = bibtexparser.load(bibtex_file, parser=parser)

def get_info_from_proceeding(crossref) :
    global bib_database
    list_info = []
    for entrycrossref in bib_database.entries :
        if entrycrossref['ENTRYTYPE']=='proceedings' :
            if entrycrossref['ID'] == crossref :
                print ("CROSSREF")
                #print ("    TITLE : ", entrycrossref['title'])
                #print ("    PUBLISHER : ", entrycrossref['publisher'])
    result = entrycrossref['title']
    return result
            

for entry in bib_database.entries :

    ########################
    ####### GESTION ARTICLES
    ########################
    if entry['ENTRYTYPE']=='article' : 
        cnt_article +=1
        print ("ARTICLE")
        type_publi = "ART"

        numero = "RI"+str(cnt_article)
        language = "en"

        #get title
        print ("TITLE:"+entry['title'])
        title = entry['title']
        title = title.replace("\n"," ")

        # get authors
        listauthors = []
        try :
            #print ("AUTHOR:"+entry['author'])
            authors = entry['author']
            list_authors_mix = authors.split("\n")
            for auth in list_authors_mix :
                lab_struct = ""
                if bib_name_user in auth :
                    auth = firstname_user+" "+name_user
                    lab_struct = labo_auth_final
                if auth[-4:] == " and" :
                    auth = auth[:-4]
                auth_full = auth.split(" ")
                prenom = auth_full[0]
                nom = auth_full[-1]
                print ("author ",auth, " - prenom ", prenom, " - nom ",nom)
                listauthors.append((nom,prenom,lab_struct))
        except KeyError :
            cnt_error_auth+=1

        # get conf
        conf = ""
        try :
            print ("JOURNAL:"+entry['journal'])
            conf = entry['journal']
            conf = conf.replace("\n"," ")
        except KeyError :
            cnt_error_jrn+=1

        town=""
        country=""
        country_acr="XX"


        # get volume
        volume = ""
        try :
            print ("VOLUME:"+entry['volume'])
            volume = entry['volume']
        except KeyError :
            cnt_error_vol+=1
#        try :
#            print ("NUMBER:"+entry['number'])
#        except KeyError :
#            cnt_error_numb+=1

        # get nb_pages
        nb_pages = ""
        try :
            print ("PAGES:"+entry['pages'])
            nb_pages = entry['pages']
        except KeyError :
            cnt_error_pages+=1

        # get Year
        year = 0
        try :
            print ("YEAR:"+entry['year'])
            year = entry['year']
        except KeyError :
            cnt_error_year_art+=1

        # get DOI
        doi_value = ""
        try :
            print ("DOI:"+entry['doi'])
            doi_value = entry['doi']
        except KeyError :
            cnt_error_doi+=1

        publisher_book = ""
        editor_book = ""



        #######################
        ### TEST EXISTING PUBLI
        #######################

        action_todo=""
        #title = title.decode("utf-8")
        titleutf8 = title.encode("utf-8")
        # Verification que la publi n'existe pas deja dans HAL
        url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(titleutf8)
        #print url_request
        req = requests.get(url_request)
        #print req.status_code
    
        json = req.json()
    
        try : 
            result = json['response']['docs']
            
            if (len(result) == 1 ) :
                # recup authors
                all_auth = ""
                try :
                    tous_authors = result[0]["authFullName_s"]
                    for auth in tous_authors:
                        print auth
                        all_auth = all_auth + auth+"-"
                except KeyError, e :
                    print "error print authors existing publi"
                resultat = resultat + "la publi n "+numero+" existe deja "+result[0]["uri_s"]+" - auteurs:"+all_auth+"\n"
                action_todo = "E"
            if (len(result) == 2 ) :
                problemes_doublon = problemes_doublon + "la publi n "+numero+" existe en plusieurs exemplaires "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"\n"
                action_todo = "2"
            if (len(result) == 0 ) :
                print "Depot sur HAL"
                action_todo = "D"
                result = False
                result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume)
                if result == True :
                    depots = depots + "publi n "+numero+" au titre "+title+" deposee dans HAL\n"
                if result == False :
                    problemes_depot = problemes_depot + "publi n "+numero+" au titre "+title+" a un probleme de depot\n"

        except KeyError :
            print ("PROBLEMES")
            action_todo = "P"
            problemes_url = problemes_url + "Probleme URL pour la publi n "+numero+" au titre "+title+"\n"



        list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))

    ########################
    ####### GESTION inproceedings
    ########################
    if entry['ENTRYTYPE']=='inproceedings' : #find inproceedings
        cnt_inproceeding +=1
        print ("INPROCEEDINGS")
        type_publi = "COMM"

        numero = "CI"+str(cnt_inproceeding)
        language = "en"

        #get title
        print ("TITLE:"+entry['title'])
        title = entry['title']
        title = title.replace("\n"," ")

        # get authors
        listauthors = []
        try :
            #print ("AUTHOR:"+entry['author'])
            authors = entry['author']
            list_authors_mix = authors.split("\n")
            for auth in list_authors_mix :
                lab_struct = ""
                if bib_name_user in auth :
                    auth = firstname_user+" "+name_user
                    lab_struct = labo_auth_final
                if auth[-4:] == " and" :
                    auth = auth[:-4]
                auth_full = auth.split(" ")
                prenom = auth_full[0]
                nom = auth_full[-1]
                print ("author ",auth, " - prenom ", prenom, " - nom ",nom)
                listauthors.append((nom,prenom,lab_struct))
        except KeyError :
            cnt_error_auth+=1

        # get conf
        conf = ""
        town=""
        country=""
        country_acr="XX"
        try :
            print ("BOOKTITLE:"+entry['booktitle'])
            booktitle = entry['booktitle']
            conf_all = booktitle.split(",")
            conf = conf_all[0]
            conf = conf.replace("\n"," ")
            #conf = conf.decode("utf-8")
            prev_conf_elmt = ""
            prev_prev_conf_elmt = ""
            for conf_elmt in conf_all :
                conf_elmt = conf_elmt.strip()
                for csv_country in list_acronym_country :
                    if conf_elmt.upper() == csv_country[1] :
                        if csv_country[1] == "USA" :
                            prev_prev_conf_elmt = prev_prev_conf_elmt.replace("\n"," ")
                            town = prev_prev_conf_elmt
                            country_acr = csv_country[0]
                            country = csv_country[1]
                            print "match country ", town,  country, country_acr
                        else :
                            prev_conf_elmt = prev_conf_elmt.replace("\n"," ")
                            town = prev_conf_elmt
                            country_acr = csv_country[0]
                            country = csv_country[1]
                            print "match country ", town,  country, country_acr
                prev_prev_conf_elmt = prev_conf_elmt
                prev_conf_elmt = conf_elmt

        except KeyError :
            cnt_error_booktitle+=1

        # get nb_pages
        nb_pages = ""
        try :
            print ("PAGES:"+entry['pages'])
            nb_pages = entry['pages']
        except KeyError :
            cnt_error_pages+=1

        # get volume
        volume = ""
        try :
            print ("VOLUME:"+entry['volume'])
            volume = entry['volume']
        except KeyError :
            cnt_error_vol+=1

        # get Year
        year = 0
        try :
            print ("YEAR:"+entry['year'])
            year = entry['year']
        except KeyError :
            cnt_error_year_inp+=1

        # get DOI
        doi_value = ""
        try :
            print ("DOI:"+entry['doi'])
            doi_value = entry['doi']
        except KeyError :
            cnt_error_doi+=1

        # get infos from crossref
        try :
            crossref = entry['crossref']
            #print ("CROSSREF:"+crossref)
            result_crossref = get_info_from_proceeding(crossref)
        except KeyError :
            cnt_error_crossref+=1

        # get Publisher
        publisher_book = ""
        try :
            print ("publisher:"+entry['publisher'])
            publisher_book = entry['publisher']
        except KeyError :
            cnt_error_publisher+=1

        # get Editor
        editor_book = ""
        try :
            print ("editor:"+entry['editor'])
            editor_book = entry['editor']
        except KeyError :
            cnt_error_editor+=1

        #######################
        ### TEST EXISTING PUBLI
        #######################

        action_todo=""
        # Verification que la publi n'existe pas deja dans HAL
        titleutf8 = title.encode("utf-8")
        url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(titleutf8)
        #print url_request
        req = requests.get(url_request)
        #print req.status_code
    
        json = req.json()
    
        try : 
            result = json['response']['docs']
            
            if (len(result) == 1 ) :
                # recup authors
                all_auth = ""
                try :
                    tous_authors = result[0]["authFullName_s"]
                    for auth in tous_authors:
                        print auth
                        all_auth = all_auth + auth+"-"
                except KeyError, e :
                    print "error print authors existing publi"
                resultat = resultat + "la publi n "+numero+" existe deja "+result[0]["uri_s"]+" - auteurs:"+all_auth+"\n"
                action_todo = "E"
            if (len(result) == 2 ) :
                problemes_doublon = problemes_doublon + "la publi n "+numero+" existe en plusieurs exemplaires "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"\n"
                action_todo = "2"
            if (len(result) == 0 ) :
                print "Depot sur HAL"
                action_todo = "D"
                result = False
                result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume)
                if result == True :
                    depots = depots + "publi n "+numero+" au titre "+title+" deposee dans HAL\n"
                if result == False :
                    problemes_depot = problemes_depot + "publi n "+numero+" au titre "+title+" a un probleme de depot\n"

        except KeyError :
            print ("PROBLEMES")
            action_todo = "P"
            problemes_url = problemes_url + "Probleme URL pour la publi n "+numero+" au titre "+title+"\n"



        list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))


    ########################
    ####### GESTION Book
    ########################
    if entry['ENTRYTYPE']=='book' : #find book
        print ("BOOK")
        cnt_book +=1
        type_publi = "OUV"
        numero = "O"+str(cnt_book)

        language = "en"

        #get title
        print ("TITLE:"+entry['title'])
        title = entry['title']
        title = title.replace("\n"," ")

        # get authors
        listauthors = []
        listauthors.append((name_user,firstname_user,labo_auth_final))
        print ("author "+name_user+" "+firstname_user)

        conf=""
        nb_pages=""
        volume = ""

        town=""
        country=""
        country_acr="XX"


        # get Year
        year = 0
        try :
            print ("YEAR:"+entry['year'])
            year = entry['year']
        except KeyError :
            cnt_error_year_art+=1

        # get DOI
        doi_value = ""
        try :
            print ("DOI:"+entry['doi'])
            doi_value = entry['doi']
        except KeyError :
            cnt_error_doi+=1

        # get Publisher
        try :
            print ("publisher:"+entry['publisher'])
            book_publisher = entry['publisher']
        except KeyError :
            cnt_error_publisher+=1

        # get Editor
        try :
            print ("editor:"+entry['editor'])
            book_editor = entry['editor']
        except KeyError :
            cnt_error_editor+=1

        #######################
        ### TEST EXISTING PUBLI
        #######################

        action_todo=""
        # Verification que la publi n'existe pas deja dans HAL
        titleutf8 = title.encode("utf-8")
        url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(titleutf8)
        #print url_request
        req = requests.get(url_request)
        #print req.status_code
    
        json = req.json()
    
        try : 
            result = json['response']['docs']
            
            if (len(result) == 1 ) :
                # recup authors
                all_auth = ""
                try :
                    tous_authors = result[0]["authFullName_s"]
                    for auth in tous_authors:
                        print auth
                        all_auth = all_auth + auth+"-"
                except KeyError, e :
                    print "error print authors existing publi"
                resultat = resultat + "la publi n "+numero+" existe deja "+result[0]["uri_s"]+" - auteurs:"+all_auth+"\n"
                action_todo = "E"
            if (len(result) == 2 ) :
                problemes_doublon = problemes_doublon + "la publi n "+numero+" existe en plusieurs exemplaires "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"\n"
                action_todo = "2"
            if (len(result) == 0 ) :
                print "Depot sur HAL"
                action_todo = "D"
                result = False
                result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume)
                if result == True :
                    depots = depots + "publi n "+numero+" au titre "+title+" deposee dans HAL\n"
                if result == False :
                    problemes_depot = problemes_depot + "publi n "+numero+" au titre "+title+" a un probleme de depot\n"

        except KeyError :
            print ("PROBLEMES")
            action_todo = "P"
            problemes_url = problemes_url + "Probleme URL pour la publi n "+numero+" au titre "+title+"\n"


        list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, type_publi, action_todo, town, country, country_acr, language))


    print ("------------------")
    cnt_total+=1



########################
####### ECRITURE CSV
########################
print "list_pub_csv length :",cnt_total
with open(file_publis_csv, 'wb') as myfile:
    myfile.write("numero|authors|title|conf/journal|nb_pages|volume|year|type_publi|TODO(E,2,D,P)|ville|pays|acr_pays|language\n")
    for pub in list_pub_csv :
        allauth = pub[1]
        allauth = allauth.replace("\n","")
        allauth = allauth.encode("utf-8") 
        title = pub[2].encode("utf-8") 
        conf = pub[3].encode("utf-8") 
        ville = pub[9].encode("utf-8")

        line = (str(pub[0])+'|'+allauth+'|'+title+'|'+conf+'|'+str(pub[4])+'|'+str(pub[5])+'|'+str(pub[6])+'|'+str(pub[7])+'|'+str(pub[8])+'|'+ville+'|'+str(pub[10])+'|'+str(pub[11])+'|'+str(pub[12]))
        myfile.write(line)
        myfile.write('\n')


print ("####### RESULTATS PARSING BIBTEX ########")
print ("cnt_article ", cnt_article)
print ("cnt_inproceeding ", cnt_inproceeding)
print ("cnt_proceeding ", cnt_proceeding)
print ("cnt_incollection ", cnt_incollection)
print ("cnt_book ", cnt_book)
print ("cnt_phdthesis ", cnt_phdthesis)
print ("cnt_total ", cnt_total)

print ("ERROR Author", cnt_error_auth)
print ("ERROR Title", cnt_error_title)

print ("-------ERRORS ARTICLE------")
print ("cnt_error_jrn",cnt_error_jrn)
print ("cnt_error_vol",cnt_error_vol)
print ("cnt_error_numb",cnt_error_numb)
print ("cnt_error_pages",cnt_error_pages)
print ("cnt_error_year_art",cnt_error_year_art)
print ("cnt_error_doi",cnt_error_doi)

print ("-------ERRORS INPROCEEDINGS------")
print ("cnt_error_booktitle:",cnt_error_booktitle)
print ("cnt_error_pages:",cnt_error_pages)
print ("cnt_error_year_inp:",cnt_error_year_inp)
print ("cnt_error_crossref:",cnt_error_crossref)
print ("cnt_error_publisher:",cnt_error_publisher)
print ("cnt_error_editor:",cnt_error_editor)

print ("-------ERRORS PROCEEDINGS------")
print ("cnt_error_idcrossref:",cnt_error_idcrossref)

print ("#########################################")

print ("######## RESULTATS XML + DEPOTS ##########")
print ("RESULTATS existants")
print resultat
print ("------------------")
print ("DEPOTS effectues")
print depots
print ("------------------")
print ("PROBLEMES DEPOTS")
print problemes_depot
print ("------------------")
print ("PROBLEMES DOUBLONS")
print problemes_doublon
print ("------------------")
print ("PROBLEMES URL")
print problemes_url

