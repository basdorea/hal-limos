#!/usr/bin/python
#-*- coding: utf-8 -*-

#from __future__ import unicode_literals
import requests
import re

# lib XML
from lxml import etree

# autres
import csv
import sys

#################################################
# script_list_to_hal.py
#
# Ce script en python 2.7 permet de rentrer des publis d'un auteur dans HAL a partir d'une liste formatee de type HCERES.
# Cet import se fait par l'API SWORD de HAL   https://api.archives-ouvertes.fr/docs/sword
#
# Principe : A partir des infos de la liste, des fichiers XML sont generes et envoyes par requete POST a une URL d'import
# 
# Necessite : un compte HAL permettant l'import, des infos sur l'auteur et sur le labo affilie, qq connaissances sur le depot sur HAL et son fonctionnement
# 
# Le script permet d'importer dans HAL des articles, des participations à des conférences, des ouvrages, des chapitres d'ouvrage et des posters
# en proposant des étapes de vérification (un fichier CSV et un depot en preproduction ); la difficulté étant que la liste soit bien formatée
# Les imports sont faits en donnant la propriete de la publi a l'auteur, le contributeur du depot etant forcement le compte qui depose
# Une methode est utilisee pour generer un XML pour les articles, confs , posters , chapitres d'ouvrage et ouvrages
# Le script principal recupere les donnes, cree le CSV et appelle cette methode
# Une verification est faite pour savoir si la publi existe deja dans HAL, si c'est le cas, pas d'import
# Deux variables booleennes permettent d'importer ou non sur une base de pré-prod et sur une base de prod
# Pour les imports, mettre ces variables a True (normalement, soit on importe sur preprod pour tester, soit en prod mais pas les deux)
# Attention, pour deposer en preprod, il est necessaire d'avoir un compte en preprod en plus du compte en prod
#
# La bonne pratique
# 
# LECTURE DE LA LISTE 
# En general, les listes sont formatées de cette facon : 
# numero-de-publi , tiret , auteurs separes par des virgules , titre entre guillements , nom de la conf/journal/... , une virgule, autres infos 
# separees par des virgules comprenant l'annee, le doi eventuellement, le nb de pages , le lieu de la conf, ...
#
# Exemples avec deux articles, une conf et un chapitre d'ouvrage:
# RI52 - HILL D., "Parallel Random Numbers, Simulation, Science and reproducibility". IEEE CISE  Computing in Science and Engineering, Invited Paper, Volume:17,  Issue: 4, 2015, pp. 66 – 71, ISSN :1521-9615  doi:10.1109/MCSE.2015.79. 
# RI51 - PASSERAT-PALMBACH J., CAUX J., SCHWEITZER P., SIREGARD P., HILL D., "Harnessing aspect oriented programming on GPU: application to warp-level parallelism (WLP)", International Journal of Computer Aided Engineering and Technology, Vol. 7, No. 2, 2015, pp. 158-175.
# CI97 - SCHWEITZER P., MAZEL C., CARLOGANU C., HILL D., "Inputs of Aspect Oriented Programming for the Profiling of C++ Parallel Applications on Manycore Platforms", Proceedings of the 2014 International Conference on High Performance Computing & Simulation, ACM/IEEE/IFIP – WIP, $Bologna Italie, IT, July 25th-29th, 2014, pp. 793-802.
# CO3 - HILL D., COQUILLARD P., "Ecological Modelling and Simulation", Handbook of Dynamic System Modeling, Paul Fishwick Editor, CRC Press, Chapter 29, pp. 29-1,18, 2007.
#
# Il sera sans doute necessaire de formater la liste en remplacant « par " , en remplacant les doubles espaces par un seul, modifier les noms de pays
# Afin de recuperer les villes et pays des conferences se trouvant un peu n'importe ou en fin de texte, j'ai choisi de fixer le caractere $ avant le nom de la ville
# 
# DONNEES RECUPEREES
# Les donnees sont recuperees ligne par ligne
# - Recup des infos du debut jusqu'au tiret, selon les lettres indiquees on retire le type de publi (RI ou RN -> article , CO -> chapitre d'ouvrage, O -> ouvrage, CI ou CN -> conference, P -> poster), on recupere aussi le numero de la publi
# - recup du tiret jusqu'au premier guillemet -> tous les auteurs separes par des virgules (dans l'ordre, nom prenom) qui sont mis dans une liste -> listauthors
# si un des noms correspond a celui de l'auteur donne en variable, on specifie le labo
# - recup du titre du depot entre les guillemets 
# - recup du texte du second guillemet jusqu'a la premiere virgule -> nom de la conf/journal , si cette valeur n'a pas ete renseignee par l'auteur (car n'a pas lieu d'etre) une valeur est quand meme recuperee mais non utilisee
# - A partir du reste de la ligne, on recupere le DOI en cherchant le texte "doi" jusqu'a la virgule suivante
# on recupere les villes et pays en cherchant du $ jusqu'au premier espace puis du premier espace jusqu'au second. Cela signifie que 
# les noms des pays doivent etre formates (en francais et sans espace , voir liste dans acronym_country.csv)
# le code du pays est retrouve a partir du fichier CSV acronym_country.csv lu en debut de script
# le nombre de pages est recupere a partir de l'element "pp." jusqu'a la prochaine virgule ou la fin de la ligne
# l'annee est recuperee par une Regex pour les annees allant de 1970 a 2029
#
# ETAPES :
# 1 - La liste est lue en plein texte et des informations en sont retirées
# 2 - Un fichier CSV est généré à partir de ces informations afin d'avoir un visuel plus propre sur les infos récupérées 
# 3 - Autant de fichiers XML sont générés que de publications avec comme nom le numero de la publi suivi de "publi.xml" -> P6publi.xml
# 4 - Si specifie, les imports des XML sont faits sur la base de préproduction et/ou la base de production
# 5 - Les resultats et erreurs éventuelles sont affichees en sortie
#
# VARIABLES :
# Certaines variables sont à modifier en début de script pour spécifier :
# - l'auteur des publis (prénom, nom, login et idHal)
# - les domaines de publication de l'auteur
# - le code du labo précédé de "struct-"
# - les infos du compte de dépôt (login et mot de passe)
# - un champ booléen pour déposer sur la base de préproduction (défaut = False)
# - un champ booléen pour déposer sur la base de production (défaut = False)
# - le nom du fichier texte contenant les publis a lire
#
# Pour info, une liste de correspondance des domaines se trouve dans le fichier list_domaines.txt
#################################################


#########################
# VARIABLES
#########################

# Auteur des publis
name_user = "HILL"
firstname_user = "David"
labo_auth_final = "struct-490706"
id_hal_user="david-rc-hill"
login_user = "davidhill"

# Domaines de l'auteur
listdomains = []
# listdomains.append("info.info-dm")
listdomains.append("info.info-se")# Genie logiciel
listdomains.append("info.info-mo")# Modelisation et simulation
listdomains.append("info.info-dc")# Calcul parallele, distribué et partegé
# listdomains.append("info.info-bi")# Bio infoormatique  

# Compte de depot
login_depot = "bdoreau"
passwd_depot = "mbrv1232"

# booleens depots
bool_depot_preprod = False
bool_depot_prod = False

# Fichier texte contenant les publis
file_publis_author = "all_publis_hill2.txt"
# Fichier CSV en sortie
file_publis_csv = "all_csv_hill.csv"

#############################################################################################################################
###################################           BUILD XML ARTICLE CONF POSTER OUV COUV
# CREATION XML pour articles, conferences, posters, ouvrages et chapitres d'ouvrage
#############################################################################################################################
#############################################################################################################################

def createXml_sendHal(numero,listauthors, lang_title, title_publi, name_conf, nb_pages, date_pub, listdomains, type_pub, ville, pays, pays_acr, doi_value, editor_book, volume):

    ## VARIABLES CREATION XML
    global name_user
    global firstname_user
    global labo_auth_final
    global id_hal_user
    global login_user

    global login_depot
    global passwd_depot

    global bool_depot_preprod
    global bool_depot_prod

    # Recup des donnes de la liste
    namefile = str(numero)+"publi.xml"

    file = open(namefile,"w") 
    file.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")


    tei = etree.Element("TEI")
    tei.set("xmlns","http://www.tei-c.org/ns/1.0")
    tei.set("xmlns_hal","http://hal.archives-ouvertes.fr/")   ### ATTENTION   remplacement xmlns_hal par xmlns:hal

    text = etree.SubElement(tei, "text")
    body = etree.SubElement(text, "body")
    listBibl = etree.SubElement(body, "listBibl")
    biblFull = etree.SubElement(listBibl, "biblFull")

    # TitleStmt
    if (type_pub == "COMM") or (type_pub == "POSTER") or  (type_pub == "ART") or (type_pub == "OUV") or  (type_pub == "COUV")  :

        titleStmt = etree.SubElement(biblFull, "titleStmt")
        title = etree.SubElement(titleStmt, "title")
        title.set("xml_lang",lang_title)          #Build from data      ### ATTENTION  remplacement xml_lang par xml:lang
        title.text = title_publi.decode("utf-8")                   #Build from data

        for auth in listauthors :
            author = etree.SubElement(titleStmt, "author")
            author.set("role","aut")
            persName = etree.SubElement(author, "persName")
            forename = etree.SubElement(persName, "forename")
            forename.set("type","first")
            forename.text = auth[1]                #Build from data
            surname = etree.SubElement(persName, "surname")
            surname.text = auth[0]                    #Build from data
            if auth[2] is not "" :
                affiliation = etree.SubElement(author, "affiliation")
                labo_author = "#"+auth[2]
                affiliation.set("ref",labo_author)             #Build from data
                if auth[1] == name_user:
                    idno_idhal =  etree.SubElement(author, "idno")
                    idno_idhal.set("type", "idhal")
                    idno.text = id_hal_user

    # Unnecessary noteStmt
    """
    notesStmt = etree.SubElement(biblFull, "notesStmt")
    note = etree.SubElement(notesStmt, "note")
    note.set("type","commentary")
    note.text = comment_publi
    """
    # sourceDesc
    sourceDesc = etree.SubElement(biblFull, "sourceDesc")
    biblStruct = etree.SubElement(sourceDesc, "biblStruct")
    analytic = etree.SubElement(biblStruct, "analytic")
    title = etree.SubElement(analytic, "title")
    title.set("xml_lang",lang_title)          #Build from data             ### ATTENTION remplacement  xml_lang par xml:lang
    title.text = title_publi.decode("utf-8")                   #Build from data

    for auth in listauthors :
        author = etree.SubElement(analytic, "author")
        author.set("role","aut")
        persName = etree.SubElement(author, "persName")
        forename = etree.SubElement(persName, "forename")
        forename.set("type","first")
        forename.text = auth[1]                #Build from data
        surname = etree.SubElement(persName, "surname")
        surname.text = auth[0]                    #Build from data
        if auth[2] is not "" :
            affiliation = etree.SubElement(author, "affiliation")
            labo_author = "#"+auth[2]
            affiliation.set("ref",labo_author)             #Build from data
            if auth[1] == name_user:
                idno_idhal =  etree.SubElement(author, "idno")
                idno_idhal.set("type", "idhal")
                idno.text = id_hal_user



    monogr = etree.SubElement(biblStruct, "monogr")
    title = etree.SubElement(monogr, "title")
    if type_pub == "ART":
        title.set("level","j")
        title.text = name_conf.decode("utf-8")                       #Build from data
    if type_pub == "COMM":
        title.set("level","m")
        title.text = name_conf.decode("utf-8")                       #Build from data
    if type_pub == "POSTER":
        title.set("level","m")
        title.text = name_conf.decode("utf-8")    
    # Pour les COUV on donne le titre du livre (ici noté name_conf)
    if type_pub == "COUV":
        title = etree.SubElement(monogr, "title")
        title.set("level","m")
        title.text = name_conf.decode("utf-8")   # ici name_conf = titre livre                    #Build from data
        if editor_book is not "" :
            editor = etree.SubElement(monogr, "editor")
            editor.text = editor_book.decode("utf-8")     #Build from data

    
    if (type_pub == "COMM") or (type_pub == "POSTER") :
        if (pays.strip() is not ""):
    
            meeting = etree.SubElement(monogr, "meeting")
            title = etree.SubElement(meeting, "title")
            title.text = name_conf.decode("utf-8")                      #Build from data

            date = etree.SubElement(meeting, "date")
            date.set("type","start")
            date.text = date_pub
            settlement = etree.SubElement(meeting, "settlement")
            settlement.text = ville.decode("utf-8")
            country = etree.SubElement(meeting, "country")
            country.set("key",pays_acr)
            country.text = pays.decode("utf-8")
    
    imprint = etree.SubElement(monogr, "imprint")
    
    if (volume is not "") :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","volume")
        biblScope.text = volume.decode("utf-8")#.strip()
    if (nb_pages is not "") :
        biblScope = etree.SubElement(imprint, "biblScope")
        biblScope.set("unit","pp")
        biblScope.text = nb_pages.decode("utf-8")#.strip()
    
    date = etree.SubElement(imprint, "date")
    date.set("type","datePub")
    date.text = date_pub

    if doi_value is not "" :
        doi = etree.SubElement(biblStruct, "idno")
        doi.set("type","doi")
        doi.text = doi_value

    # profileDesc
    profileDesc = etree.SubElement(biblFull, "profileDesc")
    langUsage = etree.SubElement(profileDesc, "langUsage")
    language = etree.SubElement(langUsage, "language")
    language.set("ident",lang_title)                     #Build from data
    textClass = etree.SubElement(profileDesc, "textClass") 
    for domain in listdomains :
        classCode = etree.SubElement(textClass, "classCode") 
        classCode.set("scheme","halDomain")
        classCode.set("n",domain)                          #Build from data
    classCode = etree.SubElement(textClass, "classCode") 
    classCode.set("scheme","halTypology")
    classCode.set("n",type_publi)

    # back 
    back = etree.SubElement(text, "back")
    listOrg = etree.SubElement(back, "listOrg")
    listOrg.set("type","structures")
    org = etree.SubElement(listOrg, "org")
    org.set("type","laboratory")
    org.set("xml_id",labo_auth_final)                                            ### ATTENTION remplacement  xml_id par xml:id


    docxml = str(etree.tostring(tei, pretty_print=True))

    file.write(docxml) 

    file.close() 

    ## CORRECTIFS

    with open(namefile, 'r') as file :
      docxml = file.read()

    # Replace the target string
    docxml = docxml.replace("xmlns_hal=","xmlns:hal=")
    docxml = docxml.replace("xml_id=","xml:id=")
    docxml = docxml.replace("xml_lang=","xml:lang=")

    # Write the file out again
    with open(namefile, 'w') as file:
        file.write(docxml)



    headers = {
        'Packaging': 'http://purl.org/net/sword-types/AOfr',
        'Content-Type': 'text/xml',
        'On-Behalf-Of': 'login|'+login_user+';idhal|'+id_hal_user,
    }


    reponse_http = False
    response = ""

    # DEPOT PREPROD
    if bool_depot_preprod == True :
        data = open(namefile)
        response = requests.post('https://api-preprod.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot))
        print "response POST : code	",response

    # DEPOT PROD
    if bool_depot_prod == True :
        data = open(namefile)
        response = requests.post('https://api.archives-ouvertes.fr/sword/hal/', headers=headers, data=data, auth=(login_depot, passwd_depot))
        print "response POST : code	",response   

    print 'request POST OK'
    if "202" in str(response) :
        reponse_http = True
    
    return reponse_http


####################################################################
########## END METHOD CREATE XML
##########
####################################################################


########################################################################################################################################
########################################################################################################################################
########## SCRIPT PRINCIPAL
########################################################################################################################################
########################################################################################################################################

resultat = ""
problemes_url = ""
problemes_doublon = ""
problemes_depot = ""
depots = ""


list_pub_csv = []

######
# lecture du fichier acronym_csv
list_acronym_country = []
with open('acronym_country.csv', 'rb') as csvfile:
    list_acron = csv.reader(csvfile, delimiter=':', quotechar='|')
    for row in list_acron:
        #print row[0], " ",row[1]
        list_acronym_country.append((row[0].upper(), row[1]))


# Lecture de la liste
list_publis = open(file_publis_author, "r")
for publi in list_publis:
    
    language = "en"
    # recup numero avant le -
    x = publi.find("-")#.decode('utf-8')
    numero = publi[0:x]
    print numero
    type_publi="TYPE"
    if ("RI") in numero :
        type_publi = "ART"

    if ("RN") in numero :
        type_publi = "ART"
        language = "fr"

    if ("O") in numero : # prend O, CO et DO
        type_publi = "OUV"

    if ("CO") in numero :
        type_publi = "COUV"

    if ("CI") in numero :
        type_publi = "COMM"

    if ("CN") in numero :
        type_publi = "COMM"
        language = "fr"

    if ("P") in numero :
        type_publi = "POSTER"

    # recup auteurs a la suite jusqu'a "
    publi = publi[x+1:]
    x = publi.find("\"")
    authors = publi[:x]
    authors = authors.strip()
    #print authors
    list_authors_mix = authors.split(",")
    listauthors = []

    # si champ vide, non pris en compte
    if list_authors_mix[-1].strip() == "" :
        del list_authors_mix[-1]

    for auth in list_authors_mix :
        auth = auth.strip()
        newauth = auth.split(" ")
        
        if (name_user in newauth[0]):
            listauthors.append((name_user,firstname_user,labo_auth_final))
        else :
            listauthors.append((newauth[0],newauth[1],""))

    #recup titre entre " et "
    publi = publi[x+1:]
    x = publi.find("\"")
    title = publi[:x]
    title = title.strip()
    #print title

    #recup conf, suppression de premiere virgule ou point
    publi = publi[x+1:]
    publi = publi.strip()
    if (publi[0] == ",") or (publi[0] == ".") :
        publi = publi[1:]
    x = publi.find(",")
    conf = publi[:x]
    conf = conf.strip()
    #print conf

    # reste
    rest_publi = publi[x+1:]

    editor_book = ""
    if (type_publi == "" ) :
        x = rest_publi.find(",")
        editor_book = rest_publi[:x]
        editor_book = editor_book.strip()

    # recup DOI
    doi_value = ""
    x = rest_publi.find("doi")
    doi_value = rest_publi[x:]
    doi_value = doi_value.strip()
    x = doi_value.find(" ")
    doi_value = doi_value[:x]
    doi_value = doi_value.strip()
    if (doi_value[-1:] == ".") or (doi_value[-1:] == ",") :
        doi_value = doi_value[:-1] 
    #print "doi_value :",doi_value
    if ("doi:" in doi_value):
        doi_value = doi_value[4:] 
    print "doi_value :",doi_value

    # recup Ville, Pays , Acron
    town = "Ville_inconnue"
    country = "Pays_inconnu"
    country_acr = "XX"
    x = rest_publi.find("$")
    towncountry = rest_publi[x:]

    towncountry = towncountry.replace(",","")
    towncountry = towncountry.replace(".","")
    x = towncountry.find(" ")
    town = towncountry[1:x]
    towncountry = towncountry[x:]
    towncountry = towncountry.strip()
    x = towncountry.find(" ")
    country = towncountry[:x]
    #towncountry = towncountry[x:]
    #towncountry = towncountry.strip()
    #x = towncountry.find(" ")
    #country_acr = towncountry[:x]
    if country is not "" :
        for country_dict, acro_dict in list_acronym_country :
            if country.upper() == country_dict :
                country_acr = acro_dict


    print " town country "+town +" "+ country+" "+country_acr

    #################
    # RECUP Nb pages + volume
    #print rest_publi
    nb_pages = ""
    x = rest_publi.find("pp.")
    nb_pages = rest_publi[x:]
    x = nb_pages.find(",")
    nb_pages = nb_pages[3:x]
    nb_pages = nb_pages.replace(".","")
    nb_pages = nb_pages.replace(",","")
    nb_pages = nb_pages.strip()
    if len(nb_pages) < 3 :
        print "PAS DE PAGES"
    else :
        print nb_pages
    
    volume = ""
    x = rest_publi.find("Vol.")
    volume = rest_publi[x:]
    x = volume.find(",")
    volume = volume[4:x]
    volume = volume.replace(".","")
    volume = volume.replace(",","")
    volume = volume.strip()
    if len(volume) < 1 :
        print "PAS DE Volume"
    else :
        print volume    
    


    # YEAR recup par regex les nombres allant de 1970 a 2029 et ne commencant pas par un nb pour eviter de trouver 2005 ds 478520051906
    try : 
        year = re.findall('\D([1][9][7-9][0-9]|[2][0][0-2][0-9])', rest_publi)[0]
    except IndexError :
        year = ""

    # recup type_publi
    #x = rest_publi.find("type:")
    #type_publi = rest_publi[x+5:]

    action_todo=""
    # Verification que la publi n'existe pas deja dans HAL
    url_request = "https://api.archives-ouvertes.fr/search/?q=title_t:\"{0}\"&fl=uri_s,halId_s,authFullName_s,authIdHal_s,title_s&wt=json".format(title)
    #print url_request
    req = requests.get(url_request)
    #print req.status_code

    json = req.json()
    

    try : 
        result = json['response']['docs']
        
        if (len(result) == 1 ) :
            # recup authors
            all_auth = ""
            try :
                tous_authors = result[0]["authFullName_s"]
                for auth in tous_authors:
                    print auth
                    all_auth = all_auth + auth+"-"
            except KeyError, e :
                print "error print authors existing publi"
            resultat = resultat + "la publi n "+numero+" existe deja "+result[0]["uri_s"]+" - auteurs:"+all_auth+"\n"
            action_todo = "E"
        if (len(result) == 2 ) :
            problemes_doublon = problemes_doublon + "la publi n "+numero+" existe en plusieurs exemplaires "+result[0]["uri_s"]+" "+result[1]["uri_s"]+"\n"
            action_todo = "2"
        if (len(result) == 0 ) :
            print "Depot sur HAL"
            action_todo = "D"
            result = False
            if (type_publi=="ART") or (type_publi=="COMM") or (type_publi=="POSTER") or (type_publi=="OUV") or (type_publi=="COUV"):
                result = createXml_sendHal(numero, listauthors, language, title, conf, nb_pages, year, listdomains, type_publi, town, country, country_acr,doi_value, editor_book, volume)
            if result == True :
                depots = depots + "publi n "+numero+" au titre "+title+" deposee dans HAL\n"
            if result == False :
                problemes_depot = problemes_depot + "publi n "+numero+" au titre "+title+" a un probleme de depot\n"
        print ("-----------")
    except KeyError :
        print ("PROBLEMES")
        action_todo = "P"
        problemes_url = problemes_url + "Probleme URL pour la publi n "+numero+" au titre "+title+"\n"
    
    list_pub_csv.append((numero,authors,title,conf,nb_pages, volume,year, rest_publi, type_publi, action_todo, town, country, country_acr, language))
    # temporaire
    # creation CSV pour rajouts
print "list_pub_csv length :",len(list_pub_csv)
with open(file_publis_csv, 'wb') as myfile:
    myfile.write("numero|authors|title|conf/journal|nb_pages|volume|year|reste|type_publi|TODO(E,2,D,P)|ville|pays|acr_pays|language\n")
    for pub in list_pub_csv :
        line = (pub[0]+'|'+pub[1]+'|'+pub[2]+'|'+pub[3]+'|'+pub[4]+'|'+pub[5]+'|'+pub[6]+'|'+pub[7]+'|'+pub[8]+'|'+pub[9]+'|'+pub[10]+'|'+pub[11]+'|'+pub[12]+'|'+pub[13])
        myfile.write(line)
        myfile.write('\n')

print ("RESULTATS existants")
print resultat
print ("------------------")
print ("DEPOTS effectues")
print depots
print ("------------------")
print ("PROBLEMES DEPOTS")
print problemes_depot
print ("------------------")
print ("PROBLEMES DOUBLONS")
print problemes_doublon
print ("------------------")
print ("PROBLEMES URL")
print problemes_url






